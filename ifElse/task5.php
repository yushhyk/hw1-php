<?php

$rating = 64;
    echo "rating = 64 <br>";
    echo "Result: ";
    if ($rating >= 0 && $rating <= 19) {
        echo "Оценка F";
    } else if ($rating >= 20 && $rating <= 39) {
        echo "Оценка E";
    } else if ($rating >= 40 && $rating <= 59) {
        echo "Оценка D";
    } else if ($rating >= 60 && $rating <= 74) {
        echo "Оценка C";
    } else if ($rating >= 75 && $rating <= 89) {
        echo "Оценка B";
    } else if ($rating >= 90 && $rating <= 100) {
        echo "Оценка A";
    } else if ($rating < 0 || $rating > 100) {
        echo "Рейтинговый балл введен неправильно";
    }

?>