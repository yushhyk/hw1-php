<?php

$array = array(5,8,4,2,32,13,65,3,-7,-4);
bubbleSort($array);
echo json_encode($array);

function bubbleSort(&$array){

    $n = count($array);

    for($i = 0; $i < $n; $i++){

        $swapped = False;

        for ($j = 0; $j < $n - $i - 1; $j++){

            if ($array[$j] > $array[$j+1]){

                $t = $array[$j];
                $array[$j] = $array[$j+1];
                $array[$j+1] = $t;
                $swapped = True;
            }
        }


        if ($swapped == False)
            break;
    }
}
?>