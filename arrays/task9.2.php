<?php

$array = array(5,8,4,2,32,13,65,3,-7,-4);
$length = count($array);

for ($i = 0; $i < $length-1; $i++) {
    $min = $i;
    for ($j = $i + 1; $j < $length; $j++) {
        if ($array[$j] < $array[$min]) {
            $min = $j;
        }
    }

    $temp = $array[$i];
    $array[$i] = $array[$min];
    $array[$min] = $temp;
}
    echo json_encode($array);
?>